require 'rspec'
require 'pry'
require_relative '../question'

describe "Question" do
  before do
    @question = Question.new(name: "mouse", text: "is it small?", answer: "Y")
  end

  it "should save a question with text" do
    expect(@question.text).to eq ("is it small?")
    expect(@question.name).to eq ("mouse")
    expect(@question.answer).to eq ("Y")
  end

  it "has yes and no attributes" do
    expect(@question.yes).to eq nil
    expect(@question.no).to eq nil
  end
end