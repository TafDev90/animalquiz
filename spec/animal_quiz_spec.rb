require 'rspec'
require 'pry'
require_relative '../animal_quiz'

describe "Quiz" do
  let(:input_stream) {StringIO.new(YES)}
  let(:output_stream) {StringIO.new}

  YES = "y\n"
  NO = "n\n"
  ANIMAL = "cat\n"

  subject do
    Quiz.new(input_stream: input_stream, output_stream: output_stream)
  end

  it "prompts user to think of an animal" do
    subject.start
    expect( output_stream.string ).to include(Quiz::INITIAL_LINE)
  end

  it "should ask the default question" do
    subject.default_question
    expect( output_stream.string ).to include(Quiz::DEFAULT_QUESTION)
  end

  context "#humble" do
    before do
      subject.you_win
    end

    it "should accept defeat" do
      expect(output_stream.string).to include(Quiz::YOU_WIN)
    end

    it "should ask for the actual animal" do
      expect(output_stream.string).to include(Quiz::HELP_ME)
    end

    # it "should ask for a differentiating question" do
    #   expect(output_stream.string).to include(Quiz::HELP_ME_QUESTION)
    # end
    #
    # it "should ask for a differentiating question answer" do
    #   expect(output_stream.string).to include(Quiz::HELP_ME_QUESTION_ANSWER)
    # end
    #
    # it "should thank player" do
    #   expect(output_stream.string).to include(Quiz::THANKS)
    # end
  end

  it "should ask if user wants to play again" do
    subject.play_again
    expect(output_stream.string).to include(Quiz::PLAY_AGAIN)
  end

  describe 'User Interaction' do
    before do
      subject.start
    end
    context "user answers yes" do
      let(:input_stream) {StringIO.new(YES)}
      it "should gloat" do
        expect(output_stream.string).to include(Quiz::GLOAT)
      end
    end

    context "user answers no" do
      let(:input_stream) {StringIO.new(NO)}
      it "should be humble" do
        expect(output_stream.string).to include(Quiz::YOU_WIN)
      end

      context "user responds" do
        let(:input_stream)  do
          user_input = []
          user_input << 'n'
          user_input << 'Rabbit'

          StringIO.new(user_input.join("\n"))
        end

        it "should ask user for the animal they were thinking" do
          expect(output_stream.string).to include(Quiz::HELP_ME)
        end

        it "should ask user a question the differentiates the animal" do
          expect(output_stream.string).to include(Quiz::HELP_ME_QUESTION)
        end
      end
    end
  end
end

