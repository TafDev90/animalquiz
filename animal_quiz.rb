require 'pry'
class Quiz

  attr_reader :input_stream, :output_stream

  INITIAL_LINE = 'Think of an animal'
  DEFAULT_QUESTION = 'Is it an elephant? (Y or N?)'
  YOU_WIN = 'You win, Help me correct my mistake before you go...'
  HELP_ME = 'What animal were you thinking of?'
  GLOAT = "I win, pretty smart huh?"
  HELP_ME_QUESTION = 'Now, can you give a question to distinguish an Elephant?'
  HELP_ME_QUESTION_ANSWER = "For a Rabbit, what is the answer to your question? (Y or N)"
  THANKS = "Thanks"
  PLAY_AGAIN = "Play again?"

  def initialize(input_stream: STDIN, output_stream: STDOUT)
    @input_stream = input_stream
    @output_stream = output_stream
  end

  def start
    output_stream.puts INITIAL_LINE
    default_question
    if yes_or_no == "y"
      output_stream.puts GLOAT
    else
      self.you_win
    end
  end

  def default_question
    output_stream.puts DEFAULT_QUESTION
  end

  def you_win
    output_stream.puts YOU_WIN
    output_stream.puts HELP_ME
    animal = input_stream.gets.chomp
    output_stream.puts HELP_ME_QUESTION
    question = input_stream.gets.chomp
    output_stream.puts HELP_ME_QUESTION_ANSWER
    answer = input_stream.gets.chomp
    output_stream.puts THANKS
  end


  def what_animal(animal1, animal2)
    puts "Now, can you give a question to distinguish an #{animal1} from #{animal2}?"
  end

  def play_again
    output_stream.puts PLAY_AGAIN
  end


  def yes_or_no
    input_stream.gets.chomp
  end
end

