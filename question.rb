class Question
  attr_reader :text, :name, :answer
  attr_accessor :yes, :no


  NO = "n"
  YES = "y"
  ROOT = "root"

  attr_accessor :value, :left, :right, :position, :parent

  def initialize(text: nil, name: nil, answer: nil)
    @text = text
    @name = name
    @answer = answer
  end
end